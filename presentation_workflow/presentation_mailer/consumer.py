import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()
# Above code loads django settings so that you can use the email system.


def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    info = json.loads(body)
    print(info)
    send_mail(
        "Your presentation has been accepted",
        f"{info['presenter_name']}, we're happy to tell you that your presentation: {info['title']}, has been accepted",
        "admin@conference.go",
        [f"{info['presenter_name']}"],
        fail_silently=False,
    )


def process_rejections(ch, method, properties, body):
    print("  Received %r" % body)
    info = json.loads(body)
    print(info)
    send_mail(
        "Your presentation has been rejected",
        f"{info['presenter_name']}, {info['title']}, has been rejected. Sorry babes.",
        "admin@conference.go",
        [f"{info['presenter_email']}"],
        fail_silently=False,
    )


while True:
    try:
        # establish connection to piika
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        # establish queues
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")
        # everything comming down the pipeline will be handled by the appropriate functions
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )

        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejections,
            auto_ack=True,
        )

        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        # wait 2 seconds and re-try while loop
        time.sleep(2.0)

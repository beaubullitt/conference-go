import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()
#


def process_rejections(ch, method, properties, body):
    print("  Received %r" % body)
    info = json.loads(body)
    print(info)
    send_mail(
        "Your presentation has been rejected",
        f"{info['presenter_name']}, {info['title']}, has been rejected. Sorry babes.",
        "admin@conference.go",
        [f"{info['presenter_email']}"],
        fail_silently=False,
    )


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejections,
    auto_ack=True,
)
channel.start_consuming()

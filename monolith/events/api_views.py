from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .acls import get_location_picture, get_weather

# CONFERENCES


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "picture_url",
        "city",
    ]


class ConferenceListEncoder(LocationListEncoder, ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


# LIST CONFERENCES FUNCTION


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        # This is the Queryset that is returned
        # Queryset must be converted to a list before using model encoder
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            safe=False,
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            # assigning new variable as location
            location = Location.objects.get(id=content["location"])
            # assign contents location to the one which already exists
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference, safe=False, encoder=ConferenceListEncoder
        )


# SHOW CONFERENCE FUNCTION
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id):
    # Below is not a queryset, but an instance.
    # I guess model encoder can take instances
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        # content = json.loads(request.body)
        # print(content)
        city = conference.location.city
        state = conference.location.state.name
        weather = get_weather(city, state)
        print(weather)
        return JsonResponse(
            {
                # include weather
                "weather": weather,
                "conference": conference,
            },
            safe=False,
            encoder=ConferenceDetailEncoder,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        # **content maps the dictionary objects to the correct
        # places in the schema
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference, safe=False, encoder=ConferenceDetailEncoder
        )


# LOCATIONS


# LIST LOCATIONS FUNCTION


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            locations,
            encoder=LocationListEncoder,
            safe=False,
        )
    # else "POST"
    else:
        # STEP 1: Decode the json into a dictionary
        content = json.loads(request.body)
        # State is a foreign key
        # STEP 3: Handle any errors that could happen
        try:
            # assign state with an abbreviation of state in content
            # STEP 2: Translate any properties into model objects
            state = State.objects.get(abbreviation=content["state"])
            # assign the state in content to that newly created
            # abbreviation object
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )
        # STEP 4: Create a new entity instance
        # PEXELS API creating pictures_url
        picture_url = get_location_picture(content["city"], content["state"])
        content.update(picture_url)

        location = Location.objects.create(**content)
        # Step 5: Retuen the new entity in a json response
        return JsonResponse(location, encoder=LocationListEncoder, safe=False)


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        # "state",
    ]
    # THis gets the extra datat (state) and returns it in an
    #  dict.
    # The dict is used by Model Encoder to update its "d"
    # with the extra values we passed in

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


# DETAILS LOCATION FUNCTION


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location, safe=False, encoder=LocationDetailEncoder
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )
        # **content maps the dictionary objects to the correct
        # places in the schema
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location, safe=False, encoder=LocationDetailEncoder
        )

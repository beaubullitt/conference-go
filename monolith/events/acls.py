from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_location_picture(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{city} {state}",
    }
    # kw arguement is when there's an equals.
    # params and headers are the kw arguements
    response = requests.get(url, params=params, headers=headers)

    picture = json.loads(response.content)
    try:
        # its [0] there because "photos" is a list whereas most other
        # things are dicts  that need to use keys to call
        return {"picture_url": picture["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather(city, state):
    url_geo = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&appid={OPEN_WEATHER_API_KEY}" # noqa: E501
    # headers = {"Authorization", OPEN_WEATHER_API_KEY}
    # params_geo = {
    #     "q": f"{city} {state}",
    #     "appid": f"{OPEN_WEATHER_API_KEY}"
    # }
    response_geo = requests.get(url_geo)
    coords = json.loads(response_geo.content)
    lat = coords[0]["lat"]
    lon = coords[0]["lon"]

    url_weather = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial" #noqa: E501
    # params_weather = {
    #     "lat": lat_lon_dict["lat"],
    #     "lon": lat_lon_dict["lon"],
    #     "appid": f"{OPEN_WEATHER_API_KEY}",
    # }
    response_weather = requests.get(url_weather)
    weather = json.loads(response_weather.content)
    try:
        print({
            # convert temp from K to F
            "temp": weather["main"]["temp"],
            "description": weather["weather"][0]["description"],
        })
        return {
            # convert temp from K to F
            "temp": weather["main"]["temp"],
            "description": weather["weather"][0]["description"],
        }

    except (KeyError, IndexError):
        return {"picture_url": None}

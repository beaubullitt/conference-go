from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            # super() method returns a TypeError if "o" is not the correct type.
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(QuerySetEncoder, DateEncoder, JSONEncoder,):
    encoders = {}

    def default(self, o):
        # checks to see if "o" (instance of confrence detail) is an instance of
        # the given model that we sepcify (aka Conference). C'mon isinstance!
        if isinstance(o, self.model):
            d = {}
            # if o has the attribute get_api_url
            if hasattr(o, "get_api_url"):
                # assign key "href" to the what the url method gives
                # in the dictionary
                d["href"] = o.get_api_url()
            for property in self.properties:
                value = getattr(o, property)
                # What does this do?
                # to get values within a value?
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
                # new method that returns empty dict
                # to be used with extra data
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}

from django.http import JsonResponse
from .models import Attendee, ConferenceV0
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class ConferenceV0DetailEncoder(ModelEncoder):
    model = ConferenceV0
    preoperties = ["name", "import_href"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name", "company_name", "created"]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees}, encoder=AttendeeDetailEncoder, safe=False
        )
    else:
        content = json.loads(request.body)

        try:
            conference = ConferenceV0.objects.get(id=conference_id)
            content["conference"] = conference
        except ConferenceV0.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )
    """
    WHEN I GO BACK AND ADD ENCODERS AND API CALLS
    else:
    content = json.loads(request.body)

    try:
        # THIS LINE IS ADDED
        conference_href = f'/api/conferences/{conference_vo_id}/'

        # THIS LINE CHANGES TO ConferenceVO and import_href
        conference = ConferenceVO.objects.get(import_href=conference_href)

        content["conference"] = conference

           ## THIS CHANGES TO ConferenceVO
    except ConferenceVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )
"""


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        # **content maps the dictionary objects to the correct
        # places in the schema
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee, safe=False, encoder=AttendeeDetailEncoder
        )
    # response = {
    #     "email": attendee.email,
    #     "name": attendee.email,
    #     "company_name": attendee.company_name,
    #     "created": attendee.created,
    #     "conference": {
    #         "name": attendee.conference.name,
    #         "href": attendee.conference.get_api_url(),
    #     },
    # }

import json
import requests
from .models import ConferenceV0


def get_conferences():
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    content = json.loads(response.content)
    for conference in content["conferences"]:
        ConferenceV0.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )
